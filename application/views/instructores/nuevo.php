<h1 class="text-center" style="color:lightblue"> <i class="mdi mdi-account-circle"></i> NUEVO INSTRUCTOR</h1>
<form class=""
id="frm_nuevo_instructor"
action="<?php echo site_url(); ?>/instructores/guardar"
method="post" enctype="multipart/form-data">
<!-- ENCTYPE="MULTIPART/FORM-DATA"  PARA QUE LOS ARCHIVOS PUEDA VIAJAR -->
<!-- //en action llamamos al site_url al controlador INSTRUCTORES y funcion GURADAR -->
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:
<span class="obligatorio">*</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control" required min="99999999"
          name="cedula_ins" value=""
          id="cedula_ins">
      </div>
      <div class="col-md-4">
          <label for="">Primer Apellido:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control" required
          name="primer_apellido_ins" value=""
          id="primer_apellido_ins">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:
          <span class="opcional">(Opcional)</span>
        </label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control" required
        name="segundo_apellido_ins" value=""
        id="segundo_apellido_ins">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control" required
          name="nombres_ins" value=""
          id="nombres_ins">
      </div>
      <div class="col-md-4">
          <label for="">Título:
            <span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          required
          name="titulo_ins" value=""
          id="titulo_ins">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:
          <span class="obligatorio">*</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        class="form-control" required
        min=""

        name="telefono_ins" value=""
        id="telefono_ins">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:
            <span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control" required
          name="direccion_ins" value=""
          id="direccion_ins">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="clo-md-11">
        <label for="">Foto:
            <span class="obligatorio">*</span>
        </label>

        <input type="file" name="foto_ins" placeholder="Ingrese el archivo"
        id="foto_ins" value="" required>

      </div>
    </div>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/instructores/index"
              class="btn btn-danger">
              <i class="mdi mdi-close"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_instructor").validate({
  rules:{
      cedula_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      primer_apellido_ins:{
        required:true,
        minlength:3,
        maxlength:50,
        letras: true,
      },
      segundo_apellido_ins:{
        Required:false,
        minlength:3,
        maxlength:50,
        letras:true
      },
      nombres_ins:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      titulo_ins:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      telefono_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      direccion_ins:{
        Required:true,
        minlength:5,
        maxlength:200,
        letras:true
      },
  },
  messages:{
    cedula_ins:{
      required:"Ingrese el número de cédula",
      minlength:"Ingrese al menos 10 números",
      maxlength:"El número de cédula solo contiene 10 digitos",
      number: "Este campo solo acepta números"
    },
    primer_apellido_ins:{
      required:"Ingrese su primer apellido",
      minlength:"Ingrese un apellido válido",
      maxlength:"Ingrese un apellido válido",
    },
    segundo_apellido_ins:{
      required:"Ingrese su segundo apellido (opcional)",
      minlength:"Ingrese un apellido válido",
      maxlength:"Ingrese un apellido válido",
    },
    nombres_ins:{
      required:"Ingrese sus nombres",
      minlength:"Ingrese un nombre válido",
      maxlength:"Ingrese un nombre válido",
    },
    titulo_ins:{
      required:"Ingrese su título académico",
      minlength:"Ingrese un título válido",
      maxlength:"Ingrese un título válido",
    },
    telefono_ins:{
      required:"Ingrese un número de teléfono válido",
      minlength:"Ingrese un número de al menos 10 dígitos",
      maxlength:"Ingrese un número de 10 dígitos",
      number:"Ingrese solo números"
    },
    direccion_ins:{
      required:"Ingrese su dirección",
      minlength:"Ingrese una dirección válida",
      maxlength:"Ingrese una dirección válida",
    }

  }
});
</script>
<script type="text/javascript">
$("#foto_ins").fileinput({language:'es'});

</script>
