<h1 class="text-center"> <i class="mdi mdi-account-circle"></i> EDITAR INSTRUCTOR</h1>
<form class=""
id="frm_editar_instructor"
action="<?php echo site_url('instructores/procesarActualizacion'); ?>"
method="post">
<!-- //en action llamamos al site_url al controlador INSTRUCTORES y funcion GURADAR -->
    <div class="row">
      <input type="hidden" name="id_ins" id="id_ins" value="<?php echo $instructorEditar->id_ins; ?>">
      <div class="col-md-4">
          <label for="">Cédula:
<span class="obligatorio">*</span>
          </label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control" required min="99999999"
          name="cedula_ins" value="<?php echo $instructorEditar->cedula_ins; ?>"
          id="cedula_ins">
      </div>
      <div class="col-md-4">
          <label for="">Primer Apellido:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control" required
          name="primer_apellido_ins" value="<?php echo $instructorEditar->primer_apellido_ins; ?>"
          id="primer_apellido_ins">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:
          <span class="opcional">(Opcional)</span>
        </label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control" required
        name="segundo_apellido_ins" value="<?php echo $instructorEditar->segundo_apellido_ins; ?>"
        id="segundo_apellido_ins">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="text"
          placeholder="Ingrese los nombres"
          class="form-control" required
          name="nombres_ins" value="<?php echo $instructorEditar->nombres_ins; ?>"
          id="nombres_ins">
      </div>
      <div class="col-md-4">
          <label for="">Título:
            <span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el titulo"
          class="form-control"
          required
          name="titulo_ins" value="<?php echo $instructorEditar->titulo_ins; ?>"
          id="titulo_ins">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:
          <span class="obligatorio">*</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        class="form-control" required
        min=""

        name="telefono_ins" value="<?php echo $instructorEditar->telefono_ins; ?>"
        id="telefono_ins">
      </div>
    </div>

    <br>
    <div class="row">
      <div class="col-md-12">
          <label for="">Dirección:
            <span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion"
          class="form-control" required
          name="direccion_ins" value="<?php echo $instructorEditar->direccion_ins; ?>"
          id="direccion_ins">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/instructores/index"
              class="btn btn-danger">
              <i class="mdi mdi-close"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_editar_instructor").validate({
  rules:{
      cedula_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      primer_apellido_ins:{
        required:true,
        minlength:3,
        maxlength:50,
        letras: true,
      },
      segundo_apellido_ins:{
        Required:false,
        minlength:3,
        maxlength:50,
        letras:true
      },
      nombres_ins:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      titulo_ins:{
        required:true,
        minlength:3,
        maxlength:100,
        letras:true
      },
      telefono_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },
      direccion_ins:{
        Required:true,
        minlength:5,
        maxlength:200,
        letras:true
      },
  },
  messages:{
    cedula_ins:{
      required:"Ingrese el número de cédula",
      minlength:"Ingrese al menos 10 números",
      maxlength:"El número de cédula solo contiene 10 digitos",
      number: "Este campo solo acepta números"
    },
    primer_apellido_ins:{
      required:"Ingrese su primer apellido",
      minlength:"Ingrese un apellido válido",
      maxlength:"Ingrese un apellido válido",
    },
    segundo_apellido_ins:{
      required:"Ingrese su segundo apellido (opcional)",
      minlength:"Ingrese un apellido válido",
      maxlength:"Ingrese un apellido válido",
    },
    nombres_ins:{
      required:"Ingrese sus nombres",
      minlength:"Ingrese un nombre válido",
      maxlength:"Ingrese un nombre válido",
    },
    titulo_ins:{
      required:"Ingrese su título académico",
      minlength:"Ingrese un título válido",
      maxlength:"Ingrese un título válido",
    },
    telefono_ins:{
      required:"Ingrese un número de teléfono válido",
      minlength:"Ingrese un número de al menos 10 dígitos",
      maxlength:"Ingrese un número de 10 dígitos",
      number:"Ingrese solo números"
    },
    direccion_ins:{
      required:"Ingrese su dirección",
      minlength:"Ingrese una dirección válida",
      maxlength:"Ingrese una dirección válida",
    }

  }
});
</script>
