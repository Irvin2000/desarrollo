<h1 class="text-center"><i class="mdi mdi-account-circle"></i> LISTADO DE INSTRUCTORES</h1>
<br>
<center>
<div class="row">
  <div class="col-md-6">

  </div>
  <a href="<?php echo site_url(); ?>/instructores/nuevo" class="btn btn-success" style="">
<i class="mdi mdi-plus"></i>
Agregar
  </a>
</div>
<br>
</center>
<?php if ($instructores): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:blue ; color:white"id="tbl_instructores">
        <thead>
           <tr>
             <th>ID</th>
             <th>FOTO</th>
             <th>CEDULA</th>
             <th>PRIMER APELLIDO</th>
             <th>SEGUNDO APELLIDO</th>
             <th>NOMBRES</th>
             <th>TITULO</th>
             <th>TELEFONO</th>
             <th>DIRECCIÓN</th>
             <th>ACCIONES</th>
           </tr>
         </thead>
         <tbody style="background-color:gray ;color:white">
           <?php foreach ($instructores as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_ins;?></td>
               <td>
               <?php if($filaTemporal->foto_ins!=""):?>
                 <img src="<?php echo base_url('uploads/').$filaTemporal->foto_ins; ?>" alt="">
               <?php else: ?>
                 N/A
               <?php endif; ?>
             </td>
               <td><?php echo $filaTemporal->cedula_ins; ?></td>
               <td><?php echo $filaTemporal->primer_apellido_ins; ?></td>
               <td><?php echo $filaTemporal->segundo_apellido_ins; ?></td>
               <td><?php echo $filaTemporal->nombres_ins; ?></td>
               <td><?php echo $filaTemporal->titulo_ins; ?></td>
               <td><?php echo $filaTemporal->telefono_ins; ?></td>
               <td><?php echo $filaTemporal->direccion_ins; ?></td>
               <td class="text-center">
               <a href="<?php echo site_url(); ?>/instructores/editar/<?php echo $filaTemporal->id_ins;?>" title="Editar">
                 <button type="submit" name="button" class="btn btn-warning">
                   <i class="mdi mdi-lead-pencil" style="color:white"></i>
                   Editar
                 </button>

                 </a>
                 &nbsp; &nbsp; &nbsp;
                 <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                   <a href="<?php echo site_url(); ?>/instructores/eliminar/<?php echo $filaTemporal->id_ins;?>" title="Eliminar">
                     <button type="submit" name="button" class="btn btn-danger">
                       <i class="mdi mdi-close-circle" style="color:white"></i>
                       Eliminar
                     </button>
                   </a>
                 <?php endif; ?>
               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>No hay instructores</h1>
       <?php endif; ?>
  </table>


<script type="text/javascript">
  $("#tbl_instructores").dataTable();
</script>
