<h1 class="text-center" style="color:lightblue"> <i class="mdi mdi-clipboard-plus"></i> NUEVO SEMINARIO</h1>
<form class=""
id="frm_nuevo_seminario"
action="<?php echo site_url(); ?>/seminarios/guardar"
method="post" enctype="multipart/form-data">
<!-- ENCTYPE="MULTIPART/FORM-DATA"  PARA QUE LOS ARCHIVOS PUEDA VIAJAR -->
<!-- //en action llamamos al site_url al controlador seminarios y funcion GURADAR -->
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:
<span class="obligatorio">*</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del seminario"
          class="form-control"name="nombre_semi_idcr" required
          id="nombre_semi_idcr">
      </div>
      <div class="col-md-4">
          <label for="">Duración:
            <span class="obligatorio">*</span>

          </label>
          <br>
          <input type="number"
          placeholder="Ingrese la duración del seminario en horas"
          class="form-control" required
          name="duracion_semi_idcr"
          id="duracion_semi_idcr">
      </div>
      <div class="col-md-4">
        <label for="">Costo:
          <span class="obligatorio">*</span>
        </label>
        <br>
        <input type="number"
        placeholder="Ingrese el costo del seminario"
        class="form-control" required
        name="costo_semi_idcr"
        id="costo_semi_idcr">
      </div>
    </div>
    <br>
  <br>
    <div class="row">
      <div class="clo-md-11">
        <label for="">Contenido:
            <span class="obligatorio">*</span>
        </label>

        <input type="file" name="doc_semi_idcr" placeholder="Contenido del seminario"
        id="doc_semi_idcr"required>

      </div>
    </div>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="mdi mdi-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/seminarios/index"
              class="btn btn-danger">
              <i class="mdi mdi-close"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_seminario").validate({
  rules:{
      nombre_semi_idcr:{
        required:true,
        minlength:3,
        maxlength:100
      },
      duracion_semi_idcr:{
        required:true,
        minlength:1,
        maxlength:3,
      },
      costo_semi_idcr:{
        Required:true,
        minlength:2,
        maxlength:4,
        digits:true
      },
      doc_semi_idcr:{
        required:true,
      },

  },
  messages:{
    nombre_semi_idcr:{
      required:"Este campo es obligatorio",
      minlength:"El nombre es muy corto",
      maxlength:"El nombre es muy extenso",
      },
    duracion_semi_idcr:{
      required:"Este campo es obligatorio",
      minlength:"Ingrese al menos un número",
      maxlength:"Ingrese menos de 100 horas",
    },
    costo_semi_idcr:{
      required:"Este campo es obligatorio",
      minlength:"Ingrese un costo válido",
      maxlength:"El costo no debe superar los 9999",
    },
    doc_semi_idcr:{
      required:"Este campo es obligatorio",
    }
  }
});
</script>
<script type="text/javascript">
$("#doc_semi_idcr").fileinput({language:'es'});

</script>
