<h1 class="text-center"><i class="mdi mdi-clipboard-check"></i> LISTADO DE SEMINARIOS</h1>
<br>
<center>
<div class="row">
  <div class="col-md-6">

  </div>
  <a href="<?php echo site_url(); ?>/seminarios/nuevo" class="btn btn-success" style="">
<i class="mdi mdi-plus"></i>
Agregar
  </a>
</div>
<br>
</center>
<?php if ($seminarios): ?>
    <table class="table table-striped table-bordered table-hover" style="background-color:white ; color:black"id="tbl_seminarios">
        <thead>
           <tr>
             <th>ID</th>
             <th>NOMBRE</th>
             <th>DURACION</th>
             <th>COSTO</th>
             <th>CONTENIDO</th>
             <th>ACCIONES</th>
           </tr>
         </thead>
         <tbody style="background-color:gray ;color:white">
           <?php foreach ($seminarios as $filaTemporal): ?>
             <tr>
               <td><?php echo $filaTemporal->id_semi_idcr;?></td>
               <td><?php echo $filaTemporal->nombre_semi_idcr; ?></td>
               <td><?php echo $filaTemporal->duracion_semi_idcr; ?></td>
               <td><?php echo $filaTemporal->costo_semi_idcr; ?></td>
               <td>
               <?php if($filaTemporal->doc_semi_idcr!=""):?>

                   <embed src="<?php echo base_url('uploads/').$filaTemporal->doc_semi_idcr; ?>" alt="" height="50px" width="50px">
               <?php else: ?>
                 N/A
               <?php endif; ?>
             </td>
               <td class="text-center">
               <a href="<?php echo site_url(); ?>/seminarios/editar/<?php echo $filaTemporal->id_semi_idcr;?>" title="Editar">
                 <button type="submit" name="button" class="btn btn-warning">
                   <i class="mdi mdi-lead-pencil" style="color:white"></i>
                   Editar
                 </button>

                 </a>
                 &nbsp; &nbsp; &nbsp;
                 <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                   <a href="<?php echo site_url(); ?>/seminarios/eliminar/<?php echo $filaTemporal->id_semi_idcr;?>" title="Eliminar">
                     <button type="submit" name="button" class="btn btn-danger">
                       <i class="mdi mdi-close-circle" style="color:white"></i>
                       Eliminar
                     </button>
                   </a>
                 <?php endif; ?>
               </td>

             </tr>
           <?php endforeach; ?>
         </tbody>
       <?php else: ?>
       <h1>No hay seminarios</h1>
       <?php endif; ?>
  </table>


<script type="text/javascript">
  $("#tbl_seminarios").dataTable();
</script>
