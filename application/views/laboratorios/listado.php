<h1>LISTADO</h1>
<?php if ($laboratorios): ?>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>CAPACIDAD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($laboratorios as $lab): ?>
        <tr>
          <td><?php echo $lab->id_lab; ?></td>
          <td><?php echo $lab->nombre_lab; ?></td>
          <td><?php echo $lab->descripcion_lab; ?></td>
          <td><?php echo $lab->capacidad_lab; ?></td>
          <td>
          Editar &nbsp;
        eliminar
      </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
