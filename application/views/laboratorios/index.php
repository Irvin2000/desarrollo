<h3>CRUD ASINCRONO
  DE LABORATORIOS</h3>
  <button type="button" name="button" onclick="cargarLaboratorios()" class="btn btn-primary">
    Refrescar
  </button>
<div class="row">
  <div class="col-md-12" id="contenedor-laboratorios">

  </div>

</div>

<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modalNuevoLaboratorio">
  <i class="mdi mdi-plus"></i>Agregar Laboratorio
</button>

<form class="" action="#" method="post" id="frm_nuevo_laboratorio">
	<div id="modalNuevoLaboratorio" class="modal fade" role="dialog">
  	<div class="modal-dialog modal-lg">
    	<!-- Modal content-->
    	<div class="modal-content">
      	<div class="modal-header">
        	<h4 class="modal-title">Nuevo Laboratorio</h4>
        	<button type="button" class="close"
        	style="color:white;"
        	data-dismiss="modal">&times;</button>
      	</div>
      	<div class="modal-body">
        	<div class="row">
          	<div class="col-md-6">
            	<label for="">NOMBRE:</label><br>
            	<input type="text" class="form-control"
            	name="nombre_lab" id="nombre_lab" value="" required> <br>
            	<label for="">CAPACIDAD ESTUDIANTES:</label><br>
            	<input type="number" class="form-control"
            	name="capacidad_lab" id="capacidad_lab" value="" required>
          	</div>
          	<div class="col-md-6">
            	<label for="">DESCRIPCION:</label><br>
            	<textarea name="descripcion_lab"
            	id="descripcion_lab"
            	class="form-control"
            	rows="8" required></textarea>
          	</div>
        	</div>
      	</div>
      	<div class="modal-footer">
        	<button type="submit" name="button" class="btn btn-primary">
            	Guardar
        	</button>
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      	</div>
    	</div>
  	</div>
	</div>
</form>
<script type="text/javascript">
  $("#frm_nuevo_laboratorio").validate({
    rules:{

    },
    messages:{

    },
    submitHandler:function(formulario){
      var datosIngresados=$(formulario).serialize();
      // alert(datosIngresados);
      $.ajax({
        url:"<?php echo site_url('laboratorios/insertarLaboratorio'); ?>",
        data:datosIngresados,
        type:'post',
        success:function(data){
          toastr.success("Laboratorio Insertado Exitosamente");
          $("#modalNuevoLaboratorio").modal("hide");
          $("#nombre_lab").val("");
          $("#capacidad_lab").val("");
          $("#descripcion_lab").val("");
        }
      });
    }
  });
</script>
<script type="text/javascript">
function cargarLaboratorios(){
  $("#contenedor-laboratorios").load("<?php echo site_url('laboratorios/listado'); ?>");
}
cargarLaboratorios();

</script>
