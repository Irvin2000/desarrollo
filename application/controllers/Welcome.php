<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('header')	;
		$this->load->view('welcome_message');
		$this->load->view('footer')	;
	}
	public function login(){
		$this->load->view('login');
	}

	public function iniciarSesion(){
	   	 $this->load->model("usuario");
	   	 $email=$this->input->post("email_usu");
	   	 $password=$this->input->post("password_usu");
	   	 $usuarioConectado=$this->usuario->obtenerPorEmailPassword($email,$password);
	   	 if($usuarioConectado){
	   					 $this->session->set_userdata("conectado",
	   					 $usuarioConectado);
							 $this->session->set_flashdata("bienvenida","Bienvenido a app cursos ".$usuarioConectado->nombre_usu.": ".$usuarioConectado->perfil_usu);
	   					 redirect("welcome/index");
	   	 }else{
	   		 redirect("welcome/login");
	   	 }
	    }

			//FUNCION PARA CERRAR CESION
			public function cerrarSesion(){
				session_destroy();
				redirect ("Welcome/login");
			}

}//CIERRE NO BORRAR
