<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instructores extends CI_Controller {
	//constructor
	function __construct()
  {
    parent::__construct();
		//cargar modelo
		$this->load->model('instructor');
		if (!$this->session->userdata("conectado")) {
			redirect ("welcome/login");
		}
	}

	public function index()
	{
		$data['instructores']=$this->instructor->obtenerTodos();
		$this->load->view('header')	;
		$this->load->view('instructores/index',$data);
		$this->load->view('footer')	;
	}
  public function nuevo()
  {
    $this->load->view('header')	;
    $this->load->view('instructores/nuevo');
    $this->load->view('footer')	;
  }
	public function guardar()
	{
		$datosNuevoInstructor=array(
			"cedula_ins"=>$this->input->post('cedula_ins'),
			"primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
			"segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
			"nombres_ins"=>$this->input->post('nombres_ins'),
			"titulo_ins"=>$this->input->post('titulo_ins'),
			"telefono_ins"=>$this->input->post('telefono_ins'),
			"direccion_ins"=>$this->input->post('direccion_ins')
		);

		$this->load->library("upload");
				$new_name = "foto_instructor_" . time() . "_" . rand(1, 5000);
				$config['file_name'] = $new_name . '_1';
				$config['upload_path']          = FCPATH . 'uploads/';
				$config['allowed_types']        = 'jpeg|jpg|png';
				$config['max_size']             = 1024*5; //5 MB
				$this->upload->initialize($config);

				if ($this->upload->do_upload("foto_ins")) {
					$dataSubida = $this->upload->data();
					$datosNuevoInstructor["foto_ins"] = $dataSubida['file_name'];
				}

	if ($this->instructor->insertar($datosNuevoInstructor)) {
		//CREACION DEL FLASH DATA
		$this->session->set_flashdata("Confirmación","Instructor guardado exitosamente");

	}else{
		$this->session->set_flashdata("error","Error al insertar instructor");
	}
	redirect('instructores/index');
		// p<rint_r($datosNuevoInstructor);
	}
//funcion para Eliminar
	public function eliminar($id_ins)
	{
		if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
			$this->session->set_flashdata("error","No tiene permisos para eliminar");
			redirect("instructores/index");
		}
		if ($this->instructor->borrar($id_ins)) {
			$this->session->set_flashdata("Confirmación","Instructor elimnado exitosamente");

		} else {
			$this->session->set_flashdata("error","Error al eliminar instructor");

		}
		redirect ('instructores/index');

	}
//FUNCION REEDERIZAR VISTA EDITAR
public function editar($id_ins){
	$data ["instructorEditar"]=$this->instructor->obtenerPorId($id_ins);
	$this->load->view('header')	;
	$this->load->view('instructores/editar',$data);
	$this->load->view('footer')	;
}

//PROCESO DE ACTUALIZACION
public function procesarActualizacion(){
	$datosEditados=array(
		"cedula_ins"=>$this->input->post('cedula_ins'),
		"primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
		"segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
		"nombres_ins"=>$this->input->post('nombres_ins'),
		"titulo_ins"=>$this->input->post('titulo_ins'),
		"telefono_ins"=>$this->input->post('telefono_ins'),
		"direccion_ins"=>$this->input->post('direccion_ins')
	);
	$id_ins=$this->input->post("id_ins");
	if ($this->instructor->actualizar($id_ins,$datosEditados)) {
		$this->session->set_flashdata("Confirmación","Instructor actualizado exitosamente");

	}else{
		$this->session->set_flashdata("error","Error al actualizar instructor");
	}
	redirect('instructores/index');

}




} // cierre de la clase
