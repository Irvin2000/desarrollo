<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seminarios extends CI_Controller {
	//constructor
	function __construct()
  {
    parent::__construct();
		//cargar modelo
		$this->load->model('seminario');
		if (!$this->session->userdata("conectado")) {
			redirect ("welcome/login");
		}
	}

	public function index()
	{
		$data['seminarios']=$this->seminario->obtenerTodos();
		$this->load->view('header')	;
		$this->load->view('seminarios/index',$data);
		$this->load->view('footer')	;
	}
  public function nuevo()
  {
    $this->load->view('header')	;
    $this->load->view('seminarios/nuevo');
    $this->load->view('footer')	;
  }
	public function guardar()
	{
		$datosNuevoSeminario=array(
			"nombre_semi_idcr"=>$this->input->post('nombre_semi_idcr'),
			"duracion_semi_idcr"=>$this->input->post('duracion_semi_idcr'),
			"costo_semi_idcr"=>$this->input->post('costo_semi_idcr'),
		);

		$this->load->library("upload");
				$new_name = "Archivo_seminario_" . time() . "_" . rand(1, 5000);
				$config['file_name'] = $new_name . '_1';
				$config['upload_path']          = FCPATH . 'uploads/';
				$config['allowed_types']        = 'doc|docx|pdf|txt';
				$config['max_size']             = 1024*6; //6 MB
				$this->upload->initialize($config);

				if ($this->upload->do_upload("doc_semi_idcr")) {
					$dataSubida = $this->upload->data();
					$datosNuevoSeminario["doc_semi_idcr"] = $dataSubida['file_name'];
				}

	if ($this->seminario->insertar($datosNuevoSeminario)) {
		//CREACION DEL FLASH DATA
		$this->session->set_flashdata("Confirmación","Seminario guardado exitosamente");

	}else{
		$this->session->set_flashdata("error","Error al insertar ");
	}
	redirect('seminarios/index');
		// p<rint_r($datosNuevoInstructor);
	}
//funcion para Eliminar
	public function eliminar($id_semi_idcr)
	{
		if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
			$this->session->set_flashdata("error","No tiene permisos para eliminar");
			redirect("seminarios/index");
		}
		if ($this->seminario->borrar($id_semi_idcr)) {
			$this->session->set_flashdata("Confirmación","Seminario elimnado exitosamente");

		} else {
			$this->session->set_flashdata("error","Error al eliminar Seminario");

		}
		redirect ('seminarios/index');

	}
//FUNCION REEDERIZAR VISTA EDITAR
public function editar($id_semi_idcr){
	$data ["semianarioEditar"]=$this->seminario->obtenerPorId($id_semi_idcr);
	$this->load->view('header')	;
	$this->load->view('seminarios/editar',$data);
	$this->load->view('footer')	;
}

//PROCESO DE ACTUALIZACION
public function procesarActualizacion(){
	$datosEditados=array(
		"nombre_semi_idcr"=>$this->input->post('nombre_semi_idcr'),
		"duracion_semi_idcr"=>$this->input->post('duracion_semi_idcr'),
		"costo_semi_idcr"=>$this->input->post('costo_semi_idcr'),
	);
	$id_semi_idcr=$this->input->post("id_semi_idcr");
	if ($this->seminario->actualizar($id_semi_idcr,$datosEditados)) {
		$this->session->set_flashdata("Confirmación","Seminario actualizado exitosamente");

	}else{
		$this->session->set_flashdata("error","Error al actualizar");
	}
	redirect('seminarios/index');

}




} // cierre de la clase
