<?php
  class Instructor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("instructor",
                $datos);
    }
    //FUNCION PARA OBTENER TODOS LOS DATOS
    function obtenerTodos(){
      $listadoInstructores=$this->db->get("instructor");//nombre de la tabla en la BDD
      //para saber si hay datos o no hay datos
      if ($listadoInstructores->num_rows()>0) { //
        return $listadoInstructores->result();
      }else{
        return false;
      }

    }
    function borrar($id_ins)
    {
      //delete from instructor where id_ins = 1:
      $this->db->where("id_ins",$id_ins);
      //return $this->db->delete("instructor");    <----- OCPION 2 para eliminar
      if ($this->db->delete("instructor")) {
        return true;
      } else {
        return false;
      }

    }

    //FUNCION PARA CONSULTAR UN Instructor
    function obtenerPorId($id_ins){
      $this->db->where("id_ins", $id_ins);
      $instructor=$this->db->get("instructor");
      if ($instructor->num_rows()>0) {
        return $instructor->row();
      } else {
        return false;
      }
    }
      //FUNCION PARA ACTUALIZAR UN INSTRUCTOR
      function actualizar($id_ins,$data){
        $this->db->where("id_ins",$id_ins);
        return $this->db->update('instructor',$data);
      }
  }//Cierre de la clase

 ?>
