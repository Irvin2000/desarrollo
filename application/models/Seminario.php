<?php
  class Seminario extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
      return $this->db->insert("seminario_idcr",$datos);
    }
    //FUNCION PARA OBTENER TODOS LOS DATOS
    function obtenerTodos(){
      $listadoSeminarios=$this->db->get("seminario_idcr");//nombre de la tabla en la BDD
      //para saber si hay datos o no hay datos
      if ($listadoSeminarios->num_rows()>0) { //
        return $listadoSeminarios->result();
      }else{
        return false;
      }

    }
    function borrar($id_semi_idcr)
    {
      //delete from instructor where id_ins = 1:
      $this->db->where("id_semi_idcr",$id_semi_idcr);
      //return $this->db->delete("instructor");    <----- OCPION 2 para eliminar
      if ($this->db->delete("seminario_idcr")) {
        return true;
      } else {
        return false;
      }

    }

    //FUNCION PARA CONSULTAR UN Instructor
    function obtenerPorId($id_semi_idcr){
      $this->db->where("id_semi_idcr", $id_semi_idcr);
      $seminario=$this->db->get("seminario_idcr");
      if ($seminario->num_rows()>0) {
        return $seminario->row();
      } else {
        return false;
      }
    }
      //FUNCION PARA ACTUALIZAR UN INSTRUCTOR
      function actualizar($id_semi_idcr,$data){
        $this->db->where("id_semi_idcr",$id_semi_idcr);
        return $this->db->update('seminario_idcr',$data);
      }
  }//Cierre de la clase

 ?>
