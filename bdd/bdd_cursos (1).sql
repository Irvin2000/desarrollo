-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-07-2023 a las 02:11:53
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdd_cursos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `id_doc` int(11) NOT NULL,
  `cedula_doc` varchar(10) NOT NULL,
  `apellido_doc` varchar(100) NOT NULL,
  `nombre_doc` varchar(100) NOT NULL,
  `titulo_doc` varchar(100) NOT NULL,
  `telefono_doc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor`
--

CREATE TABLE `instructor` (
  `id_ins` int(11) NOT NULL,
  `cedula_ins` varchar(15) DEFAULT NULL,
  `primer_apellido_ins` varchar(150) DEFAULT NULL,
  `segundo_apellido_ins` varchar(150) DEFAULT NULL,
  `nombres_ins` varchar(150) DEFAULT NULL,
  `titulo_ins` varchar(150) DEFAULT NULL,
  `telefono_ins` varchar(150) DEFAULT NULL,
  `direccion_ins` varchar(500) DEFAULT NULL,
  `foto_ins` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `instructor`
--

INSERT INTO `instructor` (`id_ins`, `cedula_ins`, `primer_apellido_ins`, `segundo_apellido_ins`, `nombres_ins`, `titulo_ins`, `telefono_ins`, `direccion_ins`, `foto_ins`) VALUES
(14, '7345342839', 'gvgghvg', 'ghvg', 'ghvgv', 'vgvgh', '8980998908', 'uyvgvghvhgv', NULL),
(16, '1756483901', 'Caiza', 'Bermudez', 'Juan', 'Licenciado', '0994537869', 'Solanda', 'foto_instructor_1688577806_3902_1.png'),
(17, '1785940376', 'Espin', 'Cadena', 'Ian', 'Ingeniero', '0886573948', 'Quito', NULL),
(18, '0503748290', 'habfhsdb', 'hbuhb', 'hubhb', 'ubuhh', '8009394409', 'nfbsabhj', 'foto_instructor_1688578021_4316_1.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorio`
--

CREATE TABLE `laboratorio` (
  `id_lab` int(11) NOT NULL,
  `nombre_lab` varchar(100) NOT NULL,
  `descripcion_lab` text NOT NULL,
  `capacidad_lab` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `laboratorio`
--

INSERT INTO `laboratorio` (`id_lab`, `nombre_lab`, `descripcion_lab`, `capacidad_lab`) VALUES
(13, 'Simulacion 2', 'erggsdf', 34),
(14, 'SIMULACION 4', 'jnfjnfas', 34),
(15, 'SIMULACION 4', 'jnfjnfas', 34),
(16, 'SIMULACION 4', 'jnfjnfas', 34),
(17, 'SIMULACION 4', 'jnfjnfas', 34),
(18, 'SIMULACION 4', 'jnfjnfas', 34),
(19, 'SIMULACION 4', 'jnfjnfas', 34),
(20, 'SIMULACION 4', 'jnfjnfas', 34),
(21, 'SIMULACION 4', 'jnfjnfas', 34),
(22, 'SIMULACION 4', 'jnfjnfas', 34),
(23, 'SIMULACION 4', 'jnfjnfas', 34),
(24, 'SIMULACION 4', 'kgyuwvhkej', 23),
(25, 'Simulacion 2', 'ghadvfhjs', 78),
(26, 'Simulacion 2', 'ghadvfhjs', 78),
(27, 'Simulacion 2', 'ghadvfhjs', 78),
(28, 'Simulacion 2', 'ghadvfhjs', 78),
(29, 'Simulacion 2', 'ghadvfhjs', 78),
(30, 'Simulacion 2', 'ghadvfhjs', 78),
(31, 'Simulacion 2', 'ghadvfhjs', 78),
(32, 'Simulacion 2', 'ghadvfhjs', 78),
(33, 'Simulacion 2', 'ghadvfhjs', 78),
(34, 'Simulacion 2', 'ghadvfhjs', 78),
(35, 'Simulacion 2', 'ghadvfhjs', 78),
(36, 'Simulacion 2', 'ghadvfhjs', 78),
(37, 'Simulacion 2', 'ghadvfhjs', 78),
(38, 'ahfbdshº', 'gvhjbjkn', 6778),
(39, 'ahfbdshº', 'gvhjbjkn', 6778),
(40, 'ahfbdshº', 'gvhjbjkn', 6778),
(41, 'ahfbdshº', 'gvhjbjkn', 6778),
(42, 'ahfbdshº', 'gvhjbjkn', 6778),
(43, 'ahfbdshº', 'gvhjbjkn', 6778),
(44, 'ahfbdshº', 'gvhjbjkn', 6778),
(45, 'ahfbdshº', 'gvhjbjkn', 6778),
(46, 'ahfbdshº', 'gvhjbjkn', 6778),
(47, 'ahfbdshº', 'gvhjbjkn', 6778),
(48, 'ahfbdshº', 'gvhjbjkn', 6778),
(49, 'ahfbdshº', 'gvhjbjkn', 6778),
(50, 'ahfbdshº', 'gvhjbjkn', 6778),
(51, 'ahfbdshº', 'gvhjbjkn', 6778),
(52, 'ahfbdshº', 'gvhjbjkn', 6778),
(53, 'ahfbdshº', 'gvhjbjkn', 6778),
(54, 'yguihj', 'gvbhnjn ', 7689);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seminario_idcr`
--

CREATE TABLE `seminario_idcr` (
  `id_semi_idcr` int(11) NOT NULL,
  `nombre_semi_idcr` varchar(100) NOT NULL,
  `duracion_semi_idcr` varchar(100) NOT NULL,
  `costo_semi_idcr` varchar(50) NOT NULL,
  `doc_semi_idcr` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `seminario_idcr`
--

INSERT INTO `seminario_idcr` (`id_semi_idcr`, `nombre_semi_idcr`, `duracion_semi_idcr`, `costo_semi_idcr`, `doc_semi_idcr`) VALUES
(4, 'Seminario de artes', '12', '13', 'Archivo_seminario_1688599842_1218_1.pdf'),
(8, 'Auditoria', '100', '29', 'Archivo_seminario_1688646665_3492_1.pdf'),
(9, 'Seminario de redes', '24', '23', 'Archivo_seminario_1688646720_1139_1.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usu` int(11) NOT NULL,
  `nombre_usu` varchar(150) DEFAULT NULL,
  `apellido_usu` varchar(150) DEFAULT NULL,
  `email_usu` varchar(150) DEFAULT NULL,
  `password_usu` varchar(500) DEFAULT NULL,
  `perfil_usu` varchar(150) DEFAULT 'SECRETARIO',
  `creacion_usu` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usu`, `nombre_usu`, `apellido_usu`, `email_usu`, `password_usu`, `perfil_usu`, `creacion_usu`) VALUES
(1, 'Irvin', 'Cadena', 'irvin@gmail.com', '123456', 'ADMINISTRADOR', '2023-07-01 07:26:21'),
(2, 'Natalia', 'Espin', 'admin@gmail.com', 'admin', 'ADMINISTRADOR', '2023-06-13 07:26:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`id_doc`);

--
-- Indices de la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`id_ins`),
  ADD UNIQUE KEY `cedula_ins` (`cedula_ins`);

--
-- Indices de la tabla `laboratorio`
--
ALTER TABLE `laboratorio`
  ADD PRIMARY KEY (`id_lab`);

--
-- Indices de la tabla `seminario_idcr`
--
ALTER TABLE `seminario_idcr`
  ADD PRIMARY KEY (`id_semi_idcr`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `docente`
--
ALTER TABLE `docente`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `instructor`
--
ALTER TABLE `instructor`
  MODIFY `id_ins` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `laboratorio`
--
ALTER TABLE `laboratorio`
  MODIFY `id_lab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `seminario_idcr`
--
ALTER TABLE `seminario_idcr`
  MODIFY `id_semi_idcr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
